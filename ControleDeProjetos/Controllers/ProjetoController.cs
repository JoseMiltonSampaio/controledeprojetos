﻿using ControleDeProjetos.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeProjetos.Controllers
{
    public class ProjetoController : Controller
    {
        DbContexto _contexto;

        public ProjetoController(DbContexto contexto)
        {
            _contexto = contexto;
        }

        public IActionResult Index()
        {
            return View(_contexto.Projeto.ToList());
        }

        [HttpGet]
        public IActionResult Editar(int id)
        {
            var projeto = _contexto.Projeto.FirstOrDefault(p => p.Id == id);
            if (projeto == null)
            {
                return NotFound();
            }

            return View(projeto);
        }

        [HttpPost]
        public IActionResult Editar(Projeto projeto)
        {
            try
            {
                var projetoEditar = _contexto.Projeto.FirstOrDefault(p => p.Id == projeto.Id);

                if (projetoEditar == null)
                {
                    return NotFound();
                }

                projetoEditar.Titulo = projeto.Titulo;
                projetoEditar.Observacao = projeto.Observacao;

                _contexto.SaveChanges();

                TempData["mensagem"] = $"{projetoEditar.Titulo} foi salvo!";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["mensagem"] = ex.Message;
                return BadRequest();
            }
        }

        public IActionResult Detalhes(int id)
        {
            return View();
        }

        public IActionResult Novo()
        {
            return View(new Projeto());
        }

        [HttpPost]
        public IActionResult Novo(Projeto projeto)
        {
            try
            {
                           
                if (ModelState.IsValid)
                {
                    _contexto.Projeto.Add(projeto);
                    _contexto.SaveChanges();
                    TempData["mensagem"] = $"{projeto.Titulo} salvo com sucesso!";
                    return RedirectToAction("Index");
                }
                else
                {
                    return View(projeto);
                }
            }
            catch (Exception ex)
            {
                TempData["mensagem"] = ex.Message;
                return BadRequest();
            }
        }
    }
}
