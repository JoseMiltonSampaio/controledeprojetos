﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeProjetos.Models
{
    public class Acao
    {
        public int Id { get; set; }
        public int ProjetoId { get; set; }
        public string Descricao { get; set; }
        public bool Concluido { get; set; }
    }
}
