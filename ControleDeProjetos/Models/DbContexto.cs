﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControleDeProjetos.Models
{
    public class DbContexto : DbContext
    {
        public DbSet<Acao> Acao { get; set; }
        public DbSet<Projeto> Projeto { get; set; }

        public DbContexto(DbContextOptions<DbContexto> opcoes) : base(opcoes)
        {

        }
    }
}
